package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @NotNull
    public List<E> findAll() {
        return entities;
    }

    @NotNull
    public E merge(@NotNull final E entity) {
        entities.add(entity);
        return entity;
    }

    public void merge(@NotNull final List<E> entities) {
        for (@NotNull final E entity: entities) merge(entity);
    }

    public void merge(@NotNull final E... entities) {
        for (@NotNull final E entity: entities) merge(entity);
    }

    public void clear() {
        entities.clear();
    }

    public void load(@NotNull final List<E> entity) {
        clear();
        merge(entity);
    }

    public void load(@NotNull final E... entity) {
        clear();
        merge(entity);
    }

}
