package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.exeption.unknown.IdUnknownException;
import ru.ovechkin.tm.exeption.unknown.IndexUnknownException;
import ru.ovechkin.tm.exeption.unknown.NameUnknownException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        entities.add(task);
    }

    @Override
    public void removeOne(@NotNull final String userId, @NotNull final Task task) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task iterator : entities) {
            if (userId.equals(iterator.getUserId())) result.add(iterator);
        }
        result.remove(task);
    }

    @NotNull
    @Override
    public List<Task> findAllUserTask(@NotNull final String userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : entities) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<Task> tasks = findAllUserTask(userId);
        this.entities.removeAll(tasks);
    }

    @Nullable
    @Override
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (id.equals(task.getId())) return task;
            }
        }
        throw new IdUnknownException();
    }

    @Nullable
    @Override
    public Task findByIndex(@NotNull final String userId,@NotNull final Integer index) {
        for (@NotNull final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (entities.indexOf(task) == index) return task;
            }
        }
        throw new IndexUnknownException(index);
    }

    @Nullable
    @Override
    public Task findByName(@NotNull final String userId,@NotNull final String name) {
        for (@NotNull final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (name.equals(task.getName())) return task;
            }
        }
        throw new NameUnknownException();
    }

    @NotNull
    @Override
    public Task removeById(@NotNull final String userId,@NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) throw new IdUnknownException();
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByIndex(@NotNull final String userId,@NotNull final Integer index) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId,@NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

}