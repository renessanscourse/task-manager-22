package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.exeption.unknown.IdUnknownException;
import ru.ovechkin.tm.exeption.unknown.IndexUnknownException;
import ru.ovechkin.tm.exeption.unknown.NameUnknownException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public final void add(@NotNull final String userId,@NotNull final Project project) {
        project.setUserId(userId);
        entities.add(project);
    }

    @Override
    public final void removeOne(@NotNull final String userId,@NotNull final Project project) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (final Project iterator: entities) {
            if (userId.equals(iterator.getUserId())) result.add(iterator);
        }
        result.remove(project);
    }

    @NotNull
    @Override
    public final List<Project> findUserProjects(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (final Project project: entities) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public final void removeAll(@NotNull final String userId) {
        @NotNull final List<Project> projects = findUserProjects(userId);
        this.entities.removeAll(projects);
    }

    @NotNull
    @Override
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Project project: entities) {
            if (userId.equals(project.getUserId())) {
                if (id.equals(project.getId())) return project;
            }
        }
        throw new IdUnknownException();
    }

    @NotNull
    @Override
    public Project findByIndex(@NotNull final String userId,@NotNull final Integer index) {
        for (@NotNull final Project project: entities) {
            if (userId.equals(project.getUserId())) {
                if (entities.indexOf(project) == index) return project;
            }
        }
        throw new IndexUnknownException(index);
    }

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId,@NotNull final String name) {
        for (@NotNull final Project project: entities) {
            if (userId.equals(project.getUserId())) {
                if (name.equals(project.getName())) return project;
            }
        }
        throw new NameUnknownException();
    }

    @NotNull
    @Override
    public Project removeById(@NotNull final String userId,@NotNull final String id) {
        @NotNull final Project project = findById(userId, id);
        entities.remove(project);
        return project;
    }

    @NotNull
    @Override
    public Project removeByIndex(@NotNull final String userId,@NotNull final Integer index) {
        @NotNull final Project project = findByIndex(userId, index);
        entities.remove(project);
        return project;
    }

    @NotNull
    @Override
    public Project removeByName(@NotNull final String userId,@NotNull final String name) {
        @NotNull final Project project = findByName(userId, name);
        entities.remove(project);
        return project;
    }

}