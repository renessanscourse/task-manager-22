package ru.ovechkin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.enumirated.Role;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity implements Serializable {

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private String middleName = "";

    @NotNull
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    public User(@NotNull String login,@NotNull String passwordHash,@NotNull Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public User(@NotNull String login,@NotNull String passwordHash,@NotNull String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{\n" +
                "login='" + login + '\n' +
                "passwordHash='" + passwordHash + '\n' +
                "email='" + email + '\n' +
                "firstName='" + firstName + '\n' +
                "lastName='" + lastName + '\n' +
                "middleName='" + middleName + '\n' +
                "role=" + role +
                "locked=" + locked +
                '}';
    }
}