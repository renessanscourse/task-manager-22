package ru.ovechkin.tm.locator;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.endpoint.*;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.endpoint.*;

@Getter
public class EndpointLocator implements IEndpointLocator {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final ISessionEndpoint sessionEndpoint;

    @NotNull
    private final IUserEndpoint userEndpoint;

    @NotNull
    private final IAuthEndpoint authEndpoint;

    @NotNull
    private final ITaskEndpoint taskEndpoint;

    @NotNull
    private final IProjectEndpoint projectEndpoint;

    @NotNull
    private final IStorageEndpoint storageEndpoint;

    public EndpointLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        sessionEndpoint = new SessionEndpoint(serviceLocator);
        userEndpoint = new UserEndpoint(serviceLocator);
        authEndpoint = new AuthEndpoint(serviceLocator);
        taskEndpoint = new TaskEndpoint(serviceLocator);
        projectEndpoint = new ProjectEndpoint(serviceLocator);
        storageEndpoint = new StorageEndpoint(serviceLocator);
    }

}