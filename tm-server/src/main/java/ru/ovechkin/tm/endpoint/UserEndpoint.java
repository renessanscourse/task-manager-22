package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.endpoint.IUserEndpoint;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    private IUserService userService;

    private ISessionService sessionService;

    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(final IServiceLocator serviceLocator) {
        super(serviceLocator);
        userService = serviceLocator.getUserService();
        sessionService = serviceLocator.getSessionService();
    }

    @Nullable
    @WebMethod
    public User findById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findById(id);
    }

    @Nullable
    @WebMethod
    public User findByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findByLogin(login);
    }

    @Nullable
    @WebMethod
    public User removeUser(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "user", partName = "user") final User user
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeUser(user);
    }

    @Nullable
    @WebMethod
    public User createWithLoginAndPassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        sessionService.validate(session);
        return userService.create(login, password);
    }

    @Nullable
    @WebMethod
    public User createWithLoginPasswordAndEmail(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email")final String email
    ) {
        sessionService.validate(session);
        return userService.create(login, password, email);
    }

    @Nullable
    @WebMethod
    public User createWithLoginPasswordAndRole(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "role", partName = "role")final Role role
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password, role);
    }

    @Nullable
    @WebMethod
    public User lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.lockUserByLogin(session.getUserId(), login);
    }

    @Nullable
    @WebMethod
    public User unLockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.unLockUserByLogin(session.getUserId(), login);
    }

    @Nullable
    @WebMethod
    public User removeById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    public User removeByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeByLogin(session.getUserId(), login);
    }

}