package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.endpoint.ITaskEndpoint;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    private ITaskService taskService;

    private ISessionService sessionService;

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(final IServiceLocator IServiceLocator) {
        super(IServiceLocator);
        this.taskService = IServiceLocator.getTaskService();
        this.sessionService = IServiceLocator.getSessionService();
    }

    @Override
    @WebMethod
    public void add(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "project", partName = "project") Task task
    ) {
        sessionService.validate(session);
        taskService.add(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void createTaskWithName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createTaskWithNameAndDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findUserTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        return taskService.findUserTasks(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        taskService.removeAllTasks(session.getUserId());
    }

    @Override
    @WebMethod
    public Task findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.findTaskById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.findTaskByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.findTaskByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(session);
        return taskService.updateTaskById(session.getUserId(), id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(session);
        return taskService.updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.removeTaskById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.removeTaskByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.removeTaskByName(session.getUserId(), name);
    }

}