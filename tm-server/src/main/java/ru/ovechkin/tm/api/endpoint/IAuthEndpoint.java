package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.Result;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthEndpoint {

    @Nullable
    @WebMethod
    String getUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    Session login(
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    Result logout(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void registry(
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "email", partName = "email") String email
    );

    @NotNull
    @WebMethod
    User showCurrentUserByUserId(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "userId", partName = "userId") String userId
    );

    @WebMethod
    void updateProfileInfo(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "newLogin", partName = "newLogin") String newLogin,
            @Nullable @WebParam(name = "newFirstName", partName = "newFirstName") String newFirstName,
            @Nullable @WebParam(name = "newMiddleName", partName = "newMiddleName") String newMiddleName,
            @Nullable @WebParam(name = "newLastName", partName = "newLastName") String newLastName,
            @Nullable @WebParam(name = "newEmail", partName = "newEmail") String newEmail
    );

    @WebMethod
    void updatePassword(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "currentPassword", partName = "currentPassword") String currentPassword,
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") String newPassword
    );

    @WebMethod
    void checkRoles(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "roles", partName = "roles") Role[] roles
    );

    @WebMethod
    boolean isAuth(@NotNull @WebParam(name = "session", partName = "session") Session session);

}