package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User add(@NotNull User user);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @NotNull
    User removeUser(@NotNull User user);

    @NotNull
    User removeById(@NotNull String id);

    @NotNull
    User removeByLogin(@NotNull String login);

}