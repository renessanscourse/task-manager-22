package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface IService<T> {

    @NotNull
    List<T> findAll();

    void load(@Nullable List<T> ts);

    void load(@Nullable T... ts);

    void mergeArray(@Nullable T... types);

    T mergeOne(@Nullable T type);

    void mergeCollection(@Nullable Collection<T> types);

    void clear();

}