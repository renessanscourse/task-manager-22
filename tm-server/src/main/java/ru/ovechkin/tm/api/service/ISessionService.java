package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;

import java.util.List;

public interface ISessionService extends IService<Session>{

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    boolean isValid(@Nullable Session session);

    @NotNull Session open(@Nullable String login, @Nullable String password);

    void validate(@Nullable Session session);

    void validate(@Nullable Session session, @Nullable Role role);

    @NotNull Session sign(@Nullable Session session);

    void signOutByLogin(@Nullable String login);

    void signOutByUserId(@Nullable String userId);

    @NotNull User getUser(@Nullable Session session) throws AccessForbiddenException;

    @NotNull String getUserId(@Nullable Session session) throws AccessForbiddenException;

    @NotNull List<Session> getListSession(@Nullable Session session) throws AccessForbiddenException;

    void close(@Nullable Session session) throws AccessForbiddenException;

    void closeAll(@Nullable Session session) throws AccessForbiddenException;

}