package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Task task);

    void remove(@Nullable String userId, @Nullable Task task);

    @Nullable
    List<Task> findUserTasks(@Nullable String userId);

    void removeTask(@Nullable String userId, @Nullable Task task);

    void removeAllTasks(@Nullable String userId);

    Task findTaskById(@Nullable String userId, @Nullable String id);

    Task findTaskByIndex(@Nullable String userId, @Nullable Integer index);

    Task findTaskByName(@Nullable String userId, @Nullable String name);

    Task updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    Task updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    Task removeTaskById(@Nullable String userId, @Nullable String id);

    Task removeTaskByIndex(@Nullable String userId, @Nullable Integer index);

    Task removeTaskByName(@Nullable String userId, @Nullable String name);

}