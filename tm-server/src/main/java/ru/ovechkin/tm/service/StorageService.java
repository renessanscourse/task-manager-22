package ru.ovechkin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.service.IStorageService;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.dto.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class StorageService implements IStorageService {

    private IServiceLocator IServiceLocator;

    public StorageService(final IServiceLocator IServiceLocator) {
        this.IServiceLocator = IServiceLocator;
    }

    @Override
    public void dataBase64Save() throws Exception {
        @Nullable final Domain domain = new Domain();
        IServiceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();

        @Nullable final byte[] bytes = byteArrayOutputStream.toByteArray();
        @Nullable final String base64 = new BASE64Encoder().encode(bytes);
        byteArrayOutputStream.close();

        @Nullable final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.close();
    }

    @Override
    public void dataBase64Load() throws Exception {
        @Nullable final String base64date = new String(Files.readAllBytes(Paths.get(PathToSavedFile.DATA_BASE64)));
        @Nullable final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64date);

        @Nullable final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @Nullable final ObjectInputStream objectInputStream  = new ObjectInputStream(byteArrayInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        IServiceLocator.getDomainService().load(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    public void dataBinarySave() throws IOException {
        @Nullable final Domain domain = new Domain();
        IServiceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_BIN);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    public void dataBinaryLoad() throws IOException, ClassNotFoundException {
        @Nullable final FileInputStream fileInputStream = new FileInputStream(PathToSavedFile.DATA_BIN);
        @Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        IServiceLocator.getDomainService().load(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    public void dataJsonJaxbSave() throws IOException, JAXBException {
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");

        @Nullable final Domain domain = new Domain();
        IServiceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_JSON_JAXB);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @Nullable final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        jaxbMarshaller.marshal(domain, file);
    }

    @Override
    public void dataJsonJaxbLoad() throws JAXBException {
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        @Nullable final File file = new File(PathToSavedFile.DATA_JSON_JAXB);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @Nullable final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);

        @Nullable final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);

        IServiceLocator.getDomainService().load(domain);
    }

    @Override
    public void dataJsonMapperLoad() throws IOException {
        @Nullable final File file = new File(PathToSavedFile.DATA_JSON_MAPPER);
        @Nullable final FileInputStream fileInputStream = new FileInputStream("./dataMapper.json");
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final Domain domain = objectMapper.readValue(file, Domain.class);

        IServiceLocator.getDomainService().load(domain);
        fileInputStream.close();
    }

    @Override
    public void dataJsonMapperSave() throws IOException {
        @Nullable final Domain domain = new Domain();
        IServiceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_JSON_MAPPER);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        @Nullable final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.close();
    }

    @Override
    public void dataXmlJaxbSave() throws IOException, JAXBException {
        @Nullable final Domain domain = new Domain();
        IServiceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_XML_JAXB);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @Nullable final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(domain, file);
    }

    @Override
    public void dataXmlJaxbLoad() throws IOException, JAXBException {
        @Nullable final File file = new File(PathToSavedFile.DATA_XML_JAXB);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);

        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final Domain domain = (Domain) unmarshaller.unmarshal(file);
        @Nullable final FileInputStream fileInputStream = new FileInputStream("./dataJaxb.xml");

        IServiceLocator.getDomainService().load(domain);
        fileInputStream.close();
    }

    @Override
    public void dataXmlMapperSave() throws IOException {
        @Nullable final Domain domain = new Domain();
        IServiceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_XML_MAPPER);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final ObjectMapper objectMapper = new XmlMapper();
        @Nullable final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        @Nullable final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.close();
    }

    @Override
    public void dataXmlMapperLoad() throws IOException {
        @Nullable final File file = new File(PathToSavedFile.DATA_XML_MAPPER);

        @Nullable final ObjectMapper xmlMapper = new XmlMapper();
        @Nullable final Domain domain = xmlMapper.readValue(file, Domain.class);

        IServiceLocator.getDomainService().load(domain);
    }

}