package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.service.IAuthService;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.other.WrongCurrentPasswordException;
import ru.ovechkin.tm.exeption.unknown.LoginUnknownException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.exeption.user.AlreadyExistLoginException;
import ru.ovechkin.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Nullable
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void checkRoles(
            @NotNull final Session session,
            @Nullable final Role[] roles
    ) {
        if (roles == null || roles.length == 0) return;
        @Nullable final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role role = user.getRole();
        for (final Role item : roles) if (role.equals(item)) return;
        throw new AccessDeniedException();
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new LoginUnknownException();
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        if (userId == null) throw new NotLoggedInException();
        userId = null;
    }

    @Override
    public void registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (userService.findByLogin(login) != null) throw new AlreadyExistLoginException(login);
        userService.create(login, password, email);
    }

    @NotNull
    public User findUserByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        return userService.findById(userId);
    }

    @Override
    public void updateProfileInfo(
            @Nullable final String newLogin,
            @Nullable final String newFirstName,
            @Nullable final String newMiddleName,
            @Nullable final String newLastName,
            @Nullable final String newEmail
    ) {
        final String userId = getUserId();
        final User user = findUserByUserId(userId);
        if (user == null) throw new UserEmptyException();
        if (newLogin == null || newLogin.isEmpty()) throw new LoginEmptyException();
        user.setLogin(newLogin);
        if (newFirstName == null || newFirstName.isEmpty()) throw new FirstNameEmptyException();
        user.setFirstName(newFirstName);
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new MiddleNameEmptyException();
        user.setMiddleName(newMiddleName);
        if (newLastName == null || newLastName.isEmpty()) throw new LastNameEmptyException();
        user.setLastName(newLastName);
        if (newEmail == null || newEmail.isEmpty()) throw new EmailEmptyException();
        user.setEmail(newEmail);
    }

    @Override
    public void updatePassword(@Nullable final String currentPassword, @Nullable final String newPassword) {
        if (currentPassword == null || currentPassword.isEmpty()) throw new PasswordEmptyException();
        if (newPassword == null || newPassword.isEmpty()) throw new PasswordEmptyException();
        @Nullable final String userId = getUserId();
        @Nullable final User user = findUserByUserId(userId);
        @Nullable final String currentPasswordCheckHash = HashUtil.salt(currentPassword);
        @NotNull final String currentUserPasswordHash = user.getPasswordHash();
        if (!currentPasswordCheckHash.equals(currentUserPasswordHash)) {
            throw new WrongCurrentPasswordException();
        }
        @Nullable final String newPasswordHash = HashUtil.salt(newPassword);
        user.setPasswordHash(newPasswordHash);
    }

}