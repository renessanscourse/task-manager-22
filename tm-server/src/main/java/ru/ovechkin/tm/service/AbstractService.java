package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IRepository;
import ru.ovechkin.tm.api.service.IService;
import ru.ovechkin.tm.entity.AbstractEntity;
import ru.ovechkin.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService <E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> entityRepository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.entityRepository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return entityRepository.findAll();
    }

    @Override
    public void load(@Nullable final List<E> es) {
        if (es == null) return;
        entityRepository.load(es);
    }

    @Override
    public void load(@Nullable final E ... es) {
        if (es == null) return;
        entityRepository.load(es);
    }

    @Override
    public void mergeArray(@Nullable E... entities) {
        if (entities == null) return;
        for (@Nullable final E entity : entities) entityRepository.merge(entity);
    }

    @Override
    public E mergeOne(@Nullable E entity) {
        if (entity == null) return null;
        entityRepository.merge(entity);
        return entity;
    }

    @Override
    public void mergeCollection(@Nullable Collection<E> entities) {
        for (@NotNull final E entity : entities) entityRepository.merge(entity);
    }

    @Override
    public void clear() {
        entityRepository.clear();
    }
}