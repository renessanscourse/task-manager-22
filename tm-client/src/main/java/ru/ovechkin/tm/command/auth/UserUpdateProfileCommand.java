package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.User;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.UPDATE_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return "Update you profile";
    }

    @Override
    public void execute() {
        @Nullable final String userId = endpointLocator.getAuthEndpoint().getUserId(session);
        @Nullable final User user = endpointLocator.getUserEndpoint().findById(session, userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[UPDATE PROFILE INFO]");
        System.out.println("YOUR CURRENT LOGIN IS: " + "[" + user.getLogin() + "]");
        System.out.print("ENTER NEW LOGIN: ");
        final String newLogin = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT FIRST NAME IS: " + "[" + user.getFirstName() + "]");
        System.out.print("ENTER NEW FIRST NAME: ");
        final String newFirstName = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT MIDDLE NAME IS: " + "[" + user.getMiddleName() + "]");
        System.out.print("ENTER NEW MIDDLE NAME: ");
        final String newMiddleName = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT LAST NAME IS: " + "[" + user.getLastName() + "]");
        System.out.print("ENTER NEW LAST NAME: ");
        final String newLastName = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT EMAIL IS: " + "[" + user.getEmail() + "]");
        System.out.print("ENTER NEW EMAIL NAME: ");
        final String newEmail = TerminalUtil.nextLine();
        endpointLocator.getAuthEndpoint().
                updateProfileInfo(
                        session,
                        newLogin,
                        newFirstName,
                        newMiddleName,
                        newLastName,
                        newEmail
                );
        System.out.println("[COMPLETE]");
    }

}