package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.AuthEndpoint;
import ru.ovechkin.tm.endpoint.Session;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.user.AlreadyLoggedInException;
import ru.ovechkin.tm.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.LOGIN;
    }

    @NotNull
    @Override
    public String description() {
        return "Login in your account";
    }

    @Override
    public void execute() {
        if (endpointLocator.getAuthEndpoint().isAuth(session)) throw new AlreadyLoggedInException();
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();
        @NotNull final Session sessionFromServer = authEndpoint.login(login, password);
        if (sessionFromServer == null) throw new UserDoesNotExistException();
        session.setId(sessionFromServer.getId());
        session.setUserId(sessionFromServer.getUserId());
        session.setSignature(sessionFromServer.getSignature());
        session.setTimestamp(sessionFromServer.getTimestamp());
        System.out.println("[OK]");
    }

}