package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.User;

public class UserShowProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.SHOW_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about your account";
    }

    @Override
    public void execute() {
        @Nullable final String userId = endpointLocator.getAuthEndpoint().getUserId(session);
        @Nullable final User user = endpointLocator.getUserEndpoint().findById(session, userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[PROFILE INFORMATION]");
        System.out.println("YOUR LOGIN IS: " + user.getLogin());
        System.out.println("YOUR FIRST NAME IS: " + user.getFirstName());
        System.out.println("YOUR MIDDLE NAME IS: " + user.getMiddleName());
        System.out.println("YOUR LAST NAME IS: " + user.getLastName());
        System.out.println("YOUR EMAIL IS: " + user.getEmail());
        System.out.println("YOUR ROLE IS: " + user.getRole());
        System.out.println("[OK]");
    }

}