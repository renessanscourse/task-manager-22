package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-user-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user's account by login";
    }

    @Override
    public void execute() {
        @Nullable final String userId = endpointLocator.getAuthEndpoint().getUserId(session);
        System.out.println("[REMOVE USER]");
        System.out.print("ENTER USER'S LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        endpointLocator.getUserEndpoint().removeByLogin(session, userId, login);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}