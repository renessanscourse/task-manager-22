package ru.ovechkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.Task;
import ru.ovechkin.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.TASK_SHOW_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = endpointLocator.getAuthEndpoint().getUserId(session);
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER TASK ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task =
                endpointLocator.getTaskEndpoint().findTaskById(session, userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[COMPLETE]");
    }

}