package ru.ovechkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.Task;
import ru.ovechkin.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.TASK_UPDATE_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = endpointLocator.getAuthEndpoint().getUserId(session);
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER TASK ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task =
                endpointLocator.getTaskEndpoint().findTaskById(session, userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.print("ENTER NEW TASK NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW TASK DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Task taskUpdated =  endpointLocator.getTaskEndpoint().
                updateTaskById(session, userId, id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

}