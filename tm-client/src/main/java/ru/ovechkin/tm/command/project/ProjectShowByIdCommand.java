package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.Project;
import ru.ovechkin.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.PROJECT_SHOW_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = endpointLocator.getAuthEndpoint().getUserId(session);
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final Project project =
                endpointLocator.getProjectEndpoint().findProjectById(session, userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[COMPLETE]");
    }

}