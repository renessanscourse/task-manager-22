package ru.ovechkin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.endpoint.Session;
import ru.ovechkin.tm.enumirated.Role;

public abstract class AbstractCommand {

    protected IEndpointLocator endpointLocator;

    protected Session session;

    public void setEndpointLocator(
            @NotNull IEndpointLocator endpointLocator,
            @NotNull Session session
    ) {
        this.endpointLocator = endpointLocator;
        this.session = session;
    }

    public Role[] roles () {
        return null;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute() throws Exception;

    public AbstractCommand() {
    }

}