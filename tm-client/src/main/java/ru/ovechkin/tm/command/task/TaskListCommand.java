package ru.ovechkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.Task;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_TASK_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        @NotNull final String userId = endpointLocator.getAuthEndpoint().getUserId(session);
        System.out.println("[LIST TASKS]");
        @NotNull final List<Task> tasks =
                endpointLocator.getTaskEndpoint().findUserTasks(session, userId);
        @NotNull int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

}