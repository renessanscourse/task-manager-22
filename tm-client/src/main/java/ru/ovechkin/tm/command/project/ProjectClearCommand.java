package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;

public final class ProjectClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_CLEAR;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        @Nullable final String userId = endpointLocator.getAuthEndpoint().getUserId(session);
        System.out.println("[CLEAR PROJECTS]");
        endpointLocator.getProjectEndpoint().removeAllProjects(session, userId);
        System.out.println("[OK]");
    }

}