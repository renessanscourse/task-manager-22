package ru.ovechkin.tm.exeption.empty;

public class DescriptionEmptyException extends RuntimeException {

    public DescriptionEmptyException() {
        super("Error! Description is empty...");
    }

}