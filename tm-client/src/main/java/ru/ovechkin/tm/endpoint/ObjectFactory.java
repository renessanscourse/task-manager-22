
package ru.ovechkin.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.ovechkin.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckRoles_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "checkRoles");
    private final static QName _CheckRolesResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "checkRolesResponse");
    private final static QName _GetUserId_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "getUserId");
    private final static QName _GetUserIdResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "getUserIdResponse");
    private final static QName _IsAuth_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "isAuth");
    private final static QName _IsAuthResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "isAuthResponse");
    private final static QName _Login_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "login");
    private final static QName _LoginResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "loginResponse");
    private final static QName _Logout_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "logout");
    private final static QName _LogoutResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "logoutResponse");
    private final static QName _Registry_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "registry");
    private final static QName _RegistryResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "registryResponse");
    private final static QName _ShowCurrentUserByUserId_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "showCurrentUserByUserId");
    private final static QName _ShowCurrentUserByUserIdResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "showCurrentUserByUserIdResponse");
    private final static QName _UpdatePassword_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "updatePassword");
    private final static QName _UpdatePasswordResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "updatePasswordResponse");
    private final static QName _UpdateProfileInfo_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "updateProfileInfo");
    private final static QName _UpdateProfileInfoResponse_QNAME = new QName("http://endpoint.tm.ovechkin.ru/", "updateProfileInfoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.ovechkin.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckRoles }
     * 
     */
    public CheckRoles createCheckRoles() {
        return new CheckRoles();
    }

    /**
     * Create an instance of {@link CheckRolesResponse }
     * 
     */
    public CheckRolesResponse createCheckRolesResponse() {
        return new CheckRolesResponse();
    }

    /**
     * Create an instance of {@link GetUserId }
     * 
     */
    public GetUserId createGetUserId() {
        return new GetUserId();
    }

    /**
     * Create an instance of {@link GetUserIdResponse }
     * 
     */
    public GetUserIdResponse createGetUserIdResponse() {
        return new GetUserIdResponse();
    }

    /**
     * Create an instance of {@link IsAuth }
     * 
     */
    public IsAuth createIsAuth() {
        return new IsAuth();
    }

    /**
     * Create an instance of {@link IsAuthResponse }
     * 
     */
    public IsAuthResponse createIsAuthResponse() {
        return new IsAuthResponse();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link Logout }
     * 
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link Registry }
     * 
     */
    public Registry createRegistry() {
        return new Registry();
    }

    /**
     * Create an instance of {@link RegistryResponse }
     * 
     */
    public RegistryResponse createRegistryResponse() {
        return new RegistryResponse();
    }

    /**
     * Create an instance of {@link ShowCurrentUserByUserId }
     * 
     */
    public ShowCurrentUserByUserId createShowCurrentUserByUserId() {
        return new ShowCurrentUserByUserId();
    }

    /**
     * Create an instance of {@link ShowCurrentUserByUserIdResponse }
     * 
     */
    public ShowCurrentUserByUserIdResponse createShowCurrentUserByUserIdResponse() {
        return new ShowCurrentUserByUserIdResponse();
    }

    /**
     * Create an instance of {@link UpdatePassword }
     * 
     */
    public UpdatePassword createUpdatePassword() {
        return new UpdatePassword();
    }

    /**
     * Create an instance of {@link UpdatePasswordResponse }
     * 
     */
    public UpdatePasswordResponse createUpdatePasswordResponse() {
        return new UpdatePasswordResponse();
    }

    /**
     * Create an instance of {@link UpdateProfileInfo }
     * 
     */
    public UpdateProfileInfo createUpdateProfileInfo() {
        return new UpdateProfileInfo();
    }

    /**
     * Create an instance of {@link UpdateProfileInfoResponse }
     * 
     */
    public UpdateProfileInfoResponse createUpdateProfileInfoResponse() {
        return new UpdateProfileInfoResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckRoles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "checkRoles")
    public JAXBElement<CheckRoles> createCheckRoles(CheckRoles value) {
        return new JAXBElement<CheckRoles>(_CheckRoles_QNAME, CheckRoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckRolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "checkRolesResponse")
    public JAXBElement<CheckRolesResponse> createCheckRolesResponse(CheckRolesResponse value) {
        return new JAXBElement<CheckRolesResponse>(_CheckRolesResponse_QNAME, CheckRolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "getUserId")
    public JAXBElement<GetUserId> createGetUserId(GetUserId value) {
        return new JAXBElement<GetUserId>(_GetUserId_QNAME, GetUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "getUserIdResponse")
    public JAXBElement<GetUserIdResponse> createGetUserIdResponse(GetUserIdResponse value) {
        return new JAXBElement<GetUserIdResponse>(_GetUserIdResponse_QNAME, GetUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsAuth }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "isAuth")
    public JAXBElement<IsAuth> createIsAuth(IsAuth value) {
        return new JAXBElement<IsAuth>(_IsAuth_QNAME, IsAuth.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsAuthResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "isAuthResponse")
    public JAXBElement<IsAuthResponse> createIsAuthResponse(IsAuthResponse value) {
        return new JAXBElement<IsAuthResponse>(_IsAuthResponse_QNAME, IsAuthResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "logout")
    public JAXBElement<Logout> createLogout(Logout value) {
        return new JAXBElement<Logout>(_Logout_QNAME, Logout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Registry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "registry")
    public JAXBElement<Registry> createRegistry(Registry value) {
        return new JAXBElement<Registry>(_Registry_QNAME, Registry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "registryResponse")
    public JAXBElement<RegistryResponse> createRegistryResponse(RegistryResponse value) {
        return new JAXBElement<RegistryResponse>(_RegistryResponse_QNAME, RegistryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowCurrentUserByUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "showCurrentUserByUserId")
    public JAXBElement<ShowCurrentUserByUserId> createShowCurrentUserByUserId(ShowCurrentUserByUserId value) {
        return new JAXBElement<ShowCurrentUserByUserId>(_ShowCurrentUserByUserId_QNAME, ShowCurrentUserByUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowCurrentUserByUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "showCurrentUserByUserIdResponse")
    public JAXBElement<ShowCurrentUserByUserIdResponse> createShowCurrentUserByUserIdResponse(ShowCurrentUserByUserIdResponse value) {
        return new JAXBElement<ShowCurrentUserByUserIdResponse>(_ShowCurrentUserByUserIdResponse_QNAME, ShowCurrentUserByUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "updatePassword")
    public JAXBElement<UpdatePassword> createUpdatePassword(UpdatePassword value) {
        return new JAXBElement<UpdatePassword>(_UpdatePassword_QNAME, UpdatePassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "updatePasswordResponse")
    public JAXBElement<UpdatePasswordResponse> createUpdatePasswordResponse(UpdatePasswordResponse value) {
        return new JAXBElement<UpdatePasswordResponse>(_UpdatePasswordResponse_QNAME, UpdatePasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProfileInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "updateProfileInfo")
    public JAXBElement<UpdateProfileInfo> createUpdateProfileInfo(UpdateProfileInfo value) {
        return new JAXBElement<UpdateProfileInfo>(_UpdateProfileInfo_QNAME, UpdateProfileInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProfileInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ovechkin.ru/", name = "updateProfileInfoResponse")
    public JAXBElement<UpdateProfileInfoResponse> createUpdateProfileInfoResponse(UpdateProfileInfoResponse value) {
        return new JAXBElement<UpdateProfileInfoResponse>(_UpdateProfileInfoResponse_QNAME, UpdateProfileInfoResponse.class, null, value);
    }

}
